const expect = require('chai').expect;
const assert = require('chai').assert;
const { filmsFile } = require('../task');
describe('Test', () => {
    it('Test_1', () => {
        assert.typeOf(filmsFile[0].name, 'string');
        assert.typeOf(filmsFile[0].genre, 'string');
        assert.typeOf(filmsFile[0].description, 'string');
    });
    it('Test_2', () => {
        expect(filmsFile[0]).to.have.all.keys('name', 'genre', 'description');
    });
    it('Test_3', () => {
        expect(filmsFile[0]).not.to.have.property('rating');
    });
    it('Test_4', () => {
        expect(filmsFile).to.have.lengthOf(3);
    });
    it('Test_5', () => {
        expect(filmsFile).to.be.an('array');
    });
});
